//
//  CommonExtension.swift
//  WeeTeam
//
//  Created by Dmitry Lavryk on 30.05.2020.
//  Copyright © 2020 Dmitry Lavryk. All rights reserved.
//

import Foundation

extension NSObject {
    
    class func className() -> String {
        
        return String(describing: self)
    }
}
