//
//  UIImageView+Help.swift
//  WeeTeam
//
//  Created by Dmitry Lavryk on 30.05.2020.
//  Copyright © 2020 Dmitry Lavryk. All rights reserved.
//

import UIKit
import Kingfisher

extension UIImageView {
    
    func setImageWith(url aUrl: URL?) {
        
        self.sm_setImageWith(url: aUrl, useActivity: true)
    }
    
    func sm_setImageWith(url aUrl: URL?, useActivity aIsUseActivity: Bool) {
        
        if aIsUseActivity {
            self.kf.indicatorType = .activity
            self.kf.indicatorType = .activity
        } else {
            
            self.kf.indicatorType = .none
        }
        
        self.kf.setImage(with: aUrl)
    }
    
    func animateWithFade(image aImage: UIImage, diration aDuration: TimeInterval = 1) {
        
        UIView.transition(with: self, duration: aDuration, options: .transitionCrossDissolve, animations: {
                            
            self.image = aImage
        }, completion: nil)
        
    }
}
