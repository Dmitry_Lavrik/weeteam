//
//  TabBarController.swift
//  WeeTeam
//
//  Created by Dmitry Lavryk on 30.05.2020.
//  Copyright © 2020 Dmitry Lavryk. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController, UITabBarControllerDelegate {
    
    private var newsVC: NewsViewController = {
        let vc: NewsViewController = NewsViewController()
        vc.tabBarItem = UITabBarItem(title: "News", image: UIImage(systemName: "list.bullet"), tag: 0)
        return vc
    }()
    
    private var favoriteVC: UIViewController = {
                let vc: FavoritesViewController = FavoritesViewController()
        vc.tabBarItem = UITabBarItem(title: "Favorite", image: UIImage(systemName: "star"), tag: 1)
        return vc
    }()
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        delegate = self
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        
        return self.selectedViewController?.preferredStatusBarStyle ?? .default
    }
    
    private func setup() {
        
        
        let viewControllers: [UIViewController] = [
            newsVC,
            favoriteVC
        ]
        
        self.viewControllers = viewControllers
    }
}

