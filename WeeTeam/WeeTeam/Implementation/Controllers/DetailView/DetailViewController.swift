//
//  DetailViewController.swift
//  WeeTeam
//
//  Created by Dmitry Lavryk on 30.05.2020.
//  Copyright © 2020 Dmitry Lavryk. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    @IBOutlet weak var imageNews: UIImageView!
    @IBOutlet weak var titleNews: UILabel!
    @IBOutlet weak var abstractNews: UILabel!
    @IBOutlet weak var urlNews: UILabel!
    
    var news: ResultModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleNews.text = news?.title
        abstractNews.text = news?.abstract
        urlNews.text = news?.url
    }
    
}
