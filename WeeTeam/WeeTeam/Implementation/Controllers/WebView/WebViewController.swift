//
//  WebViewController.swift
//  WeeTeam
//
//  Created by Dmitry Lavryk on 30.05.2020.
//  Copyright © 2020 Dmitry Lavryk. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController {
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var webWiew: WKWebView!
    
    // MARK: - Public properties
    
    var stringURL: String = ""
    
    // MARK: - Override
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(addTapped))
        
        setWebView()
    }
    
    // MARK: - Public methods
    
    private func setWebView() {
        
        guard let newsURl = URL(string: stringURL) else { return }
        let newsWebsite = URLRequest(url: newsURl)
        
        webWiew.load(newsWebsite)
    }
    
    @objc private func addTapped() {
        
        navigationController?.popViewController(animated: true)
    }
}
