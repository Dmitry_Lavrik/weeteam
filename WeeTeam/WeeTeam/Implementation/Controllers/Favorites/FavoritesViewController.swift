//
//  FavoritesViewController.swift
//  WeeTeam
//
//  Created by Dmitry Lavryk on 30.05.2020.
//  Copyright © 2020 Dmitry Lavryk. All rights reserved.
//

import UIKit

class FavoritesViewController: BaseNewsViewController {
    
    // MARK: - Override
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        removeRefreshControl()
        isHiddenFavoriteButton = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        getFavoriteNews()
    }
    
    // MARK: - Private methods
    
    private func getFavoriteNews() {
        
        CoreDataService.getCoreData { [weak self] (favoriteNews, error) in
            
            if let error = error {
                
                self?.showAlertWith(title: error.localizedDescription)
                
            } else {
                
                guard let favoriteNews = favoriteNews else {
                    return
                }
                
                self?.newsArray = favoriteNews
                self?.tableView.reloadData()
                
                if favoriteNews.count == 0 {
                    
                    self?.showAlertWith(title: "favorite is Empty")
                }
            }
        }
    }
    
    private func removeRefreshControl() {
        
        if refreshControl.isDescendant(of: tableView) {
            
            refreshControl.removeFromSuperview()
        }
    }
}
