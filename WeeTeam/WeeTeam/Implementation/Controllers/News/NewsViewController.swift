//
//  NewsViewController.swift
//  WeeTeam
//
//  Created by Dmitry Lavryk on 30.05.2020.
//  Copyright © 2020 Dmitry Lavryk. All rights reserved.
//

import UIKit

enum NewsCategories: Int {
    case emailed, shared, viewed
}

class NewsViewController: BaseNewsViewController {
    
    // MARK: - IBOutlet

    @IBOutlet weak var newsCategories: UISegmentedControl!
    
    // MARK: - Private properties
    
    private var urlNews = NewsPaths.Request.emailed.url()
    
    // MARK: - Override
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getNews(urlNews)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    @objc override func refreshData() {

        getNews(urlNews)
    }
    
    // MARK: - IBAction
    
    @IBAction func pressedNewsCategories(_ sender: UISegmentedControl) {
        
        let selectedSegmentNews = NewsCategories(rawValue: sender.selectedSegmentIndex)
        
        switch selectedSegmentNews {
        case .emailed:
            urlNews = NewsPaths.Request.emailed.url()
        case .shared:
            urlNews = NewsPaths.Request.shared.url()
        case .viewed:
            urlNews = NewsPaths.Request.viewed.url()
        case .none:
            break
        }
        
        getNews(urlNews)
    }
}
