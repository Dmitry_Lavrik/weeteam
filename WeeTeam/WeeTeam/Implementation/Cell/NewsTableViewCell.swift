//
//  NewsTableViewCell.swift
//  WeeTeam
//
//  Created by Dmitry Lavryk on 30.05.2020.
//  Copyright © 2020 Dmitry Lavryk. All rights reserved.
//

import UIKit

class NewsTableViewCell: BaseTableViewCell {
    
    // MARK: - IBOutlet
    
    @IBOutlet weak private var favoriteButton: UIButton!
    @IBOutlet weak private var titleLabel: UILabel!
    @IBOutlet weak private var descriptionLabel: UILabel!
    @IBOutlet weak private var photoImage: UIImageView! {
        
        didSet {
            
            photoImage.layer.masksToBounds = true
            photoImage.layer.cornerRadius = photoImage.frame.size.width / 2
        }
    }
    
    // MARK: - Public properties
    
    var isHiddenFavoriteButton: Bool = false {
        
        didSet {
            
            favoriteButton.isHidden = isHiddenFavoriteButton
        }
    }
    
    var setNews: ResultModel? {
        
        didSet {
            
            guard let news = setNews else {
                return
            }
            
            titleLabel.text = news.title
            descriptionLabel.text = news.abstract
            
            if let stringURL = news.media?.first?.mediaMetadata?.last?.url {
                
                photoImage.setImageWith(url: URL(string: stringURL))
            }
        }
    }
    
    // MARK: - Private methods
    
    @IBAction private func pressedFavoriteButton(_ sender: UIButton) {
        
        if let news = setNews {
            
            CoreDataService.saveCoreData(news)
        }
    }
}
