//
//  BaseTableViewCell.swift
//  WeeTeam
//
//  Created by Dmitry Lavryk on 30.05.2020.
//  Copyright © 2020 Dmitry Lavryk. All rights reserved.
//

import UIKit

class BaseTableViewCell: UITableViewCell {

        class func nib() -> UINib
    {
        return UINib.init(nibName: self.className(), bundle: Bundle.main)
    }
    
    class func identifier() -> String
    {
        return self.className()
    }
}
