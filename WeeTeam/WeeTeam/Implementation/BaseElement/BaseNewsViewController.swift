//
//  BaseViewController.swift
//  WeeTeam
//
//  Created by Dmitry Lavryk on 30.05.2020.
//  Copyright © 2020 Dmitry Lavryk. All rights reserved.
//

import UIKit

class BaseNewsViewController: UIViewController {
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Public properties
    
    let refreshControl = UIRefreshControl()
    
    var newsArray: [ResultModel] = []
    var isHiddenFavoriteButton: Bool = false
    
    // MARK: - Override
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setTableView()
        
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
    }
    
    // MARK: - Private method
    
    private func setTableView() {
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.addSubview(refreshControl)
        
        tableView.register(NewsTableViewCell.nib(), forCellReuseIdentifier: NewsTableViewCell.identifier())
    }
    
    func getNews(_ url: String) {
        
        NewsService.getNews(url: url) { [weak self] (emailedNews, error) in
            
            self?.refreshControl.endRefreshing()
            
            if let error = error {
                
                self?.showAlertWith(title: error.localizedDescription)
                
            } else {
                
                guard let news = emailedNews else { return }
                self?.newsArray = news
                self?.tableView.reloadData()
            }
        }
    }
    
    // MARK: - Public method
    
    @objc func refreshData() {}
}

// MARK: - UITableViewDelegate, UITableViewDataSource

extension BaseNewsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: NewsTableViewCell.identifier()) as? NewsTableViewCell
        
        cell?.setNews = newsArray[indexPath.row]
        cell?.isHiddenFavoriteButton = isHiddenFavoriteButton
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self is NewsViewController {
            
            if let stringURL = newsArray[indexPath.row].url {
                
                let webVC = WebViewController()
                webVC.stringURL = stringURL
                
                navigationController?.pushViewController(webVC, animated: true)
            }
            
        } else {
            
            if self is FavoritesViewController {
                
                let news = newsArray[indexPath.row]
                let detailVC = DetailViewController()
                detailVC.news = news
                
                navigationController?.pushViewController(detailVC, animated: true)
            }
        }
    }
}
