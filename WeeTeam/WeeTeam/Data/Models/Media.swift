//
//  Media.swift
//  WeeTeam
//
//  Created by Dmitry Lavryk on 30.05.2020.
//  Copyright © 2020 Dmitry Lavryk. All rights reserved.
//

import Foundation

struct Media : Codable {
    
    let approvedForSyndication : Int?
    let caption : String?
    let copyright : String?
    let mediaMetadata : [MediaMetadatum]?
    let subtype : String?
    let type : String?
    
    enum CodingKeys: String, CodingKey {
        case approvedForSyndication = "approved_for_syndication"
        case caption = "caption"
        case copyright = "copyright"
        case mediaMetadata = "media-metadata"
        case subtype = "subtype"
        case type = "type"
    }
    
    init(mediaMetadata: [MediaMetadatum]) {
        self.approvedForSyndication = 0
        self.caption = ""
        self.copyright = ""
        self.mediaMetadata = mediaMetadata
        self.subtype = ""
        self.type = ""
    }
}
