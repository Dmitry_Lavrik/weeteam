//
//  ResultModel.swift
//  WeeTeam
//
//  Created by Dmitry Lavryk on 30.05.2020.
//  Copyright © 2020 Dmitry Lavryk. All rights reserved.
//

import Foundation

struct ResultModel: Codable {
    
    let abstract : String?
    let adxKeywords : String?
    let assetId : Int?
    let byline : String?
    let column : String?
    let countType : String?
    let emailCount : Int?
    let etaId : Int?
    let id : Int?
    let media : [Media]?
    let nytdsection : String?
    let publishedDate : String?
    let section : String?
    let source : String?
    let subsection : String?
    let title : String?
    let type : String?
    let updated : String?
    let uri : String?
    let url : String?
    
    enum CodingKeys: String, CodingKey {
        case abstract = "abstract"
        case adxKeywords = "adx_keywords"
        case assetId = "asset_id"
        case byline = "byline"
        case column = "column"
        case countType = "count_type"
        case emailCount = "email_count"
        case etaId = "eta_id"
        case id = "id"
        case media = "media"
        case nytdsection = "nytdsection"
        case publishedDate = "published_date"
        case section = "section"
        case source = "source"
        case subsection = "subsection"
        case title = "title"
        case type = "type"
        case updated = "updated"
        case uri = "uri"
        case url = "url"
    }
    
    init(title: String, abstract: String, url: String, media: [Media]) {
        self.abstract = abstract
        self.adxKeywords = ""
        self.assetId = 0
        self.byline = ""
        self.column = ""
        self.countType = ""
        self.emailCount = 0
        self.etaId = 0
        self.id = 0
        self.media = media
        self.nytdsection = ""
        self.publishedDate = ""
        self.section = ""
        self.source = ""
        self.subsection = ""
        self.title = title
        self.type = ""
        self.updated = ""
        self.uri = ""
        self.url = url
    }
}
