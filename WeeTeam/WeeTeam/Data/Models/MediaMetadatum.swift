//
//  MediaMetadatum.swift
//  WeeTeam
//
//  Created by Dmitry Lavryk on 30.05.2020.
//  Copyright © 2020 Dmitry Lavryk. All rights reserved.
//

import Foundation

struct MediaMetadatum : Codable {
    
    let format : String?
    let height : Int?
    let url : String?
    let width : Int?
    
    enum CodingKeys: String, CodingKey {
        case format = "format"
        case height = "height"
        case url = "url"
        case width = "width"
    }
    
    init(url: String) {
        self.format = ""
        self.height = 0
        self.url = url
        self.width = 0
    }
}
