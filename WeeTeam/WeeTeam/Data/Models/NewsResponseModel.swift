//
//  NewsResponseModel.swift
//  WeeTeam
//
//  Created by Dmitry Lavryk on 30.05.2020.
//  Copyright © 2020 Dmitry Lavryk. All rights reserved.
//

import Foundation

struct NewsResponseModel : Codable {
    
    let copyright : String?
    let numResults : Int?
    let results : [ResultModel]?
    let status : String?
    
    enum CodingKeys: String, CodingKey {
        case copyright = "copyright"
        case numResults = "num_results"
        case results = "results"
        case status = "status"
    }
}
