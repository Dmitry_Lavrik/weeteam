//
//  RequestManager.swift
//  WeeTeam
//
//  Created by Dmitry Lavryk on 30.05.2020.
//  Copyright © 2020 Dmitry Lavryk. All rights reserved.
//

import Foundation
import Alamofire

public enum ResultRequest {
    case success(Data)
    case error(Error)
}

public typealias JSON = [ String : Any ]
public typealias completion = (ResultRequest) -> Void

class RequestManager {
    
    static let shared = RequestManager()
    
    // MARK: - Request functions
    
    func request(url: String, parameters: JSON?, requestMethod: HTTPMethod, completion: @escaping completion) {
        
        AF.request(url, method: requestMethod, parameters: parameters, encoding: JSONEncoding.default).responseData {
            
            [weak self] response in
            guard let strongSelf = self else {
                completion(.error(RequestUtilities.getEmptyError()))
                return
            }
            strongSelf.parseResponse(response: response, completion: completion)
        }
    }

    // MARK: - Parse functions
    
    func parseResponse(response: AFDataResponse<Data>, completion: @escaping completion) {
        
        guard let statusCode = response.response?.statusCode else {
            completion(.error(RequestUtilities.getEmptyError()))
            return
        }
        
        if statusCode == 200 {
            
            if let data = response.data {
                
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                    print(">>> REQUEST <<< ", response.request?.url?.absoluteString ?? "Empty URL", json)
                    
                        completion(.success(data))
                    
                } catch let error {
                    
                    completion(.error(error))
                }
            } else {
                
                completion(.error(RequestUtilities.getEmptyError()))
            }
            
        } else if statusCode == 401 {
            
            completion(.error(RequestUtilities.getEmptyError()))
        } else {
            
            completion(.error(RequestUtilities.getErrorWithMessageAndCode(statusCode, message: "Server error")))
        }
    }
}
