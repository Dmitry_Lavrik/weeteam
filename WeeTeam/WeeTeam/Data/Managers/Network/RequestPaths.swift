//
//  RequestPaths.swift
//  WeeTeam
//
//  Created by Dmitry Lavryk on 30.05.2020.
//  Copyright © 2020 Dmitry Lavryk. All rights reserved.
//

import Foundation

// MARK: - Domains

public enum BaseURL {
    static let testDomain = "https://api.nytimes.com/svc/mostpopular/v2"
}

// MARK: - NewsPaths

public enum NewsPaths {
    
    enum Request: String {
        case emailed = "/emailed/30.json?api-key="
        case shared = "/shared/30.json?api-key="
        case viewed = "/viewed/30.json?api-key="
        
        func url() -> String {
            return BaseURL.testDomain + rawValue + APIKey.key
        }
    }
}
