//
//  RequestUtilities.swift
//  WeeTeam
//
//  Created by Dmitry Lavryk on 30.05.2020.
//  Copyright © 2020 Dmitry Lavryk. All rights reserved.
//

import Foundation

// MARK: - Utilities

class RequestUtilities {
    
    // MARK: - ERRORS
    
    class func getErrorWithMessageAndCode(_ code: Int, message: String) -> NSError {
        
        let error = NSError(domain: BaseURL.testDomain, code: code, userInfo: [NSLocalizedDescriptionKey: message])
        return error
    }
    
    class func getEmptyError() -> NSError {
        
        let error = NSError(domain: BaseURL.testDomain, code: -3, userInfo: [NSLocalizedDescriptionKey: "Empty error"])
        return error
    }
}
