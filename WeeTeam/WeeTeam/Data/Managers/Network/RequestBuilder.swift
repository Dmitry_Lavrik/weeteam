//
//  RequestBuilder.swift
//  WeeTeam
//
//  Created by Dmitry Lavryk on 30.05.2020.
//  Copyright © 2020 Dmitry Lavryk. All rights reserved.
//

import Foundation

enum Result<T> {
    case success(T)
    case error(Error)
}

typealias NewsResponseHandler = (Result<NewsResponseModel>) -> ()

class RequestBuilder {
    
    // MARK: - News
    
    class func getNews(url: String, completion: @escaping NewsResponseHandler) {
        
        RequestManager.shared.request(url: url, parameters: nil, requestMethod: .get) { (result) in
            
            switch result {
            case let .success(data):
                do {
                    
                    let result: NewsResponseModel = try JSONDecoder().decode(NewsResponseModel.self, from: data)
                    completion(.success(result))
                } catch {
                    
                    completion(.error(error))
                    
                } case let .error(error):
                completion(.error(error))
            }
        }
    }
}
