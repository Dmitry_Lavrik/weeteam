//
//  NewsService.swift
//  WeeTeam
//
//  Created by Dmitry Lavryk on 30.05.2020.
//  Copyright © 2020 Dmitry Lavryk. All rights reserved.
//

import Foundation

class NewsService {
    
    // MARK: - News
    
    class func getNews(url: String, completion: @escaping ([ResultModel]?, Error?) -> ()) {
        
        RequestBuilder.getNews(url: url) { (result) in

            switch result
            {
            case let .success(news):
                completion(news.results, nil)
            case let .error(error):
                completion(nil, error)
            }
        }
    }
}
