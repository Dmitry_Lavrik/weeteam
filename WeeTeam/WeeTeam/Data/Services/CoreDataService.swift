//
//  CoreDataService.swift
//  WeeTeam
//
//  Created by Dmitry Lavryk on 30.05.2020.
//  Copyright © 2020 Dmitry Lavryk. All rights reserved.
//

import UIKit
import CoreData

class CoreDataService {
    
    static var contex = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    class func saveCoreData(_ news: ResultModel) {
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "News")
        let fetchResults = try! contex.fetch(fetchRequest) as! [News]
        let ids = fetchResults.map{ $0.id }
        
        guard !ids.contains(Int64(news.id!)) else {
            
            print("this news is already in CoreData")
            return
        }
        
        guard
            let title = news.title,
            let abstract = news.abstract,
            let url = news.url,
            let id = news.id,
            let photoURL = news.media?.first?.mediaMetadata?.last?.url else { return }
        
        let newsObject = News(context: contex)
        let mediaObject = MediaNews(context: contex)
        let mediaMetadata = MediaMetadata(context: contex)
        
        mediaObject.mediaMetadata = mediaMetadata
        newsObject.id = Int64(id)
        newsObject.media = mediaObject
        newsObject.title = title
        newsObject.abstract = abstract
        newsObject.newsURL = url
        mediaMetadata.urlPhoto = photoURL
        
        do {
            try contex.save()
            print("Save BD")
        } catch {
            print(error.localizedDescription)
        }
    }
    
    class func getCoreData(completion: @escaping ([ResultModel]?, Error?) -> ()) {
        
        let fetchRequest: NSFetchRequest<News> = News.fetchRequest()
        
        do {
            var newsArray: [ResultModel] = []
            
            let news = try contex.fetch(fetchRequest)
            
            news.forEach { (news) in
                
                newsArray.append(ResultModel(title: news.title!,
                                             abstract: news.abstract!,
                                             url: news.newsURL!,
                                             media: [Media(mediaMetadata: [MediaMetadatum(url: news.media!.mediaMetadata!.urlPhoto!)])]))
            }
            
            completion(newsArray, nil)
            
        } catch {
            
            print(error.localizedDescription)
            completion(nil, error)
        }
    }
}
